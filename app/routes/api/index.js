const Base = require('../base')
const Product = require('./product')

class ApiRoute extends Base {
  _setupRoute() {
    this.router.use('/product', Product)
  }
}

module.exports = new ApiRoute().router
