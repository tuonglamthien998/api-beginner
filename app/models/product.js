const mongoose = require('./base')

const Product = mongoose.model('product', {
  name: String,
  image: String,
  description: String,
  quantity: Number,
  price: Number,
  status: {
    type: String,
    enum: ['ACTIVE', 'PENDING', 'EXPIRED', 'LOCKED'],
    defaut: 'PENDING',
  },
})

module.exports = Product
