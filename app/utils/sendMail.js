const sgMail = require('@sendgrid/mail')
const dotenv = require('dotenv');

dotenv.config();

sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const SENDER = 'vbros.st@gmail.com'

sendEmailFromContact = async (data) => {
  try {
    const msg = {
      from: data.email,
      to: SENDER,
      subject: data.subject || `Contact from ${data.name}`,
      text: data.message,
    }
    const res = await sgMail.send(msg)
    return res[0].statusCode;
  } catch (error) {
    console.log(error)
  }
}

module.exports = { sendEmailFromContact }

