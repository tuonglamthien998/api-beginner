const Base = require('../base')
const ProductController = require('../../controllers/ProductController')

class ProductRoute extends Base {
  _setupRoute() {
    this.router.get('/', this._handleRoute(ProductController.getAll))
    // this.router.get('/', this._handleRoute(ProductController.create))
  }
}

module.exports = new ProductRoute().router
