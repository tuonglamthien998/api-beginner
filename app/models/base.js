const mongoose = require('mongoose')

mongoose.connect(process.env.DB || 'mongodb://localhost:27017/api-beginner', {
  useNewUrlParser: true,
})

mongoose.Promise = Promise

module.exports = mongoose
